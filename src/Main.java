import java.io.IOException;
import java.lang.String;
import java.sql.*;
import java.util.Scanner;

public class Main {


//    private final String url = "jdbc:postgresql://localhost/clinica?currentSchema=clinica";
//    private final String user = "postgres";
//    private final String password = "@Projeto";

    private final String url = "jdbc:postgresql://10.27.133.186/bd_trabalho?currentSchema=clinica_katharine_gabriel_paulo";
    private final String user = "aluno";
    private final String password = "aluno";


    public static void primeiraConsulta(Connection conexao) throws SQLException{
        String sql = "SELECT (pnome || ' ' || snome) AS nome, cpf, data_nascimento, telefone_residencial " +
                "FROM cliente WHERE data_nascimento > '1980-01-01'";

        PreparedStatement comando = conexao.prepareStatement(sql);

        System.out.println("[ 1 ] Listar o nome completo, o CPF, a data de nascimento e o " +
                "\n\t  telefone residencial dos clientes que nasceram depois de " +
                "\n\t  1º de janeiro de 1980.");

        //System.out.println("Executando consulta: " + sql + '\n');

        ResultSet resultado = comando.executeQuery();

        System.out.format("| %20s | %20s | %20s | %20s | \n",
                "--------------------", "--------------------", "--------------------", "--------------------");

        System.out.format("| %20s | %20s | %20s | %20s | \n",
                "Código", "Cpf", "Data de Nascimento", "Telefone");

        System.out.format("| %20s | %20s | %20s | %20s | \n",
                "--------------------", "--------------------", "--------------------", "--------------------");

        while(resultado.next()){


            String codigo = resultado.getString("nome");

            long cpf = resultado.getLong("cpf");
            Date data_nascimento = resultado.getDate("data_nascimento");
            String telefone = resultado.getString("telefone_residencial");

            System.out.printf("| %20s | %20s | %20s | %20s | \n",codigo, cpf, data_nascimento, telefone);

        }


    }

    public static void segundaConsulta(Connection conexao) throws SQLException{
        String sql = "SELECT pnome || ' ' || snome AS nome, crmv, salario " +
                "FROM funcionario NATURAL JOIN veterinario " +
                "WHERE salario::numeric > 6000.00";

        PreparedStatement comando = conexao.prepareStatement(sql);

        System.out.println("[ 2 ] Listar o nome completo, o CRMV e o salário dos funcionários no " +
                "\n\t  cargo de veterinário que ganham mais de R$ 6000.00.");

        //System.out.println('\n' + "Executando consulta: " + sql + '\n');

        ResultSet resultado = comando.executeQuery();

        System.out.format("| %20s | %20s | %20s | \n",
                "--------------------", "--------------------", "--------------------");

        System.out.format("| %20s | %20s | %20s | \n",
                "Código", "CRMV", "Salário");

        System.out.format("| %20s | %20s | %20s | \n",
                "--------------------", "--------------------", "--------------------");

        while(resultado.next()){

            String codigo = resultado.getString("nome");
            String crmv = resultado.getString("crmv");
            String salario = resultado.getString("salario");
            System.out.printf("| %20s | %20s | %20s | \n",codigo, crmv, salario);
        }

    }


    public static void terceiraConsulta(Connection conexao) throws SQLException{
        String sql = "(SELECT \n" +
                "    c.idconsulta,\n" +
                "    c.status,\n" +
                "    c.idveterinario,\n" +
                "    v.crmv,\n" +
                "    f.pnome,\n" +
                "    f.snome,\n" +
                "    c.resumo\n" +
                "FROM\n" +
                "    consulta c\n" +
                "        INNER JOIN\n" +
                "    veterinario v ON c.idveterinario = v.idfuncionario\n" +
                "        INNER JOIN\n" +
                "    funcionario f ON c.idveterinario = f.idfuncionario\n" +
                "WHERE\n" +
                "    crmv = 'CRMV-SE 237495')\n" +
                "UNION (SELECT \n" +
                "\t    c.idconsulta,\n" +
                "\t    c.status,\n" +
                "\t    c.idveterinario,\n" +
                "\t    v.crmv,\n" +
                "\t    f.pnome,\n" +
                "\t    f.snome,\n" +
                "\t    c.resumo\n" +
                "\tFROM\n" +
                "\t    consulta c\n" +
                "\t\tINNER JOIN\n" +
                "\t    veterinario v ON c.idveterinario = v.idfuncionario\n" +
                "\t\tINNER JOIN\n" +
                "\t    funcionario f ON c.idveterinario = f.idfuncionario\n" +
                "\tWHERE\n" +
                "\t    crmv = 'CRMV-SE 468415'\n" +
                "\t)";

        PreparedStatement comando = conexao.prepareStatement(sql);

        System.out.println("[ 3 ] Listar o identificador, o status e o resumo de todas as " +
                "\n\t  consultas (realizadas e agendadas) dos veterinários com " +
                "\n\t  CRMV  468415 e 237495 bem como seus nomes completos.");


        //System.out.println('\n' + "Executando consulta: " + sql + '\n');

        ResultSet resultado = comando.executeQuery();

        System.out.format("| %10s | %20s | %20s | %20s | %20s | %20s | %20s\n",
                "----------", "--------------------", "--------------------",
                "--------------------", "--------------------", "--------------------",
                "--------------------");

        System.out.format("| %10s | %20s | %20s | %20s | %20s | %20s | %20s\n",
                "Nº cons.", "Status", "Cód. do Veterinário", "CRMV", "Nome", "Sobrenome",  "Resumo");

        System.out.format("| %10s | %20s | %20s | %20s | %20s | %20s | %20s\n",
                "----------", "--------------------", "--------------------",
                "--------------------", "--------------------", "--------------------",
                "--------------------");

        while(resultado.next()){
            int idconsulta = resultado.getInt("idconsulta");
            String status = resultado.getString("status");
            int idveterinario = resultado.getInt("idveterinario");
            String crmv = resultado.getString("crmv");
            String pnome = resultado.getString("pnome");
            String snome = resultado.getString("snome");
            String resumo = resultado.getString("resumo");
            System.out.printf("| %10d | %20s | %20d | %20s | %20s | %20s | %20s \n",idconsulta, status, idveterinario,
                    crmv, pnome, snome, resumo );
        }

    }


    public static void quartaConsulta(Connection conexao) throws SQLException{
        String sql = "SELECT \n" +
                "    f.idfornecedor,\n" +
                "    f.nome_real,\n" +
                "    f.nome_fantasia,\n" +
                "    SUM(val_compra) as valor\n" +
                "FROM\n" +
                "    produto p\n" +
                "        JOIN\n" +
                "    fornecedor_has_produto fhp USING (idproduto)\n" +
                "        INNER JOIN\n" +
                "    fornecedor f ON fhp.idfornecedor = f.idfornecedor\n" +
                "GROUP BY f.idfornecedor\n" +
                "ORDER BY f.idfornecedor ";

        PreparedStatement comando = conexao.prepareStatement(sql);

        System.out.println("[ 4 ] Listar os identificadores, os nomes reais e fantasia dos " +
                "\n\t  fornecedores e os valores de todas as compras realizadas " +
                "\n\t  de todos os itens fornecidos.");


        //System.out.println('\n' + "Executando consulta: " + sql + '\n');

        ResultSet resultado = comando.executeQuery();

        System.out.format("| %20s | %25s | %25s | %20s | \n",
                "--------------------", "-------------------------", "-------------------------",
                "--------------------");

        System.out.format("| %20s | %25s | %25s | %20s | \n",
                "Número do Fornecedor", "Nome Real", "Nome Fantasia", "Valor de Compra");

        System.out.format("| %20s | %25s | %25s | %20s | \n",
                "--------------------", "-------------------------", "-------------------------",
                "--------------------");

        while(resultado.next()){
            int idfornecedor = resultado.getInt("idfornecedor");
            String nome_real = resultado.getString("nome_real");
            String nome_fantasia = resultado.getString("nome_fantasia");
            String val_compra = resultado.getString("valor");
            System.out.printf("| %20d | %25s | %25s | %20s | \n",idfornecedor, nome_real, nome_fantasia,
                    val_compra);
        }

    }


    public static void quintaConsulta(Connection conexao) throws SQLException{
        String sql = "SELECT \n" +
                "    cs.idconsulta,\n" +
                "    v.crmv,\n" +
                "    cl.cpf,\n" +
                "    s.valor,\n" +
                "    cs.valor as valor_materiais\n" +
                "FROM\n" +
                "\t(SELECT SUM(val_revenda) as valor, c.idconsulta\n" +
                "     \t\tFROM produto p \n" +
                "                 INNER JOIN\n" +
                "                    itemproduto ip ON (ip.idproduto = p.idproduto)\n" +
                "                 INNER JOIN\n" +
                "                     consulta c ON (c.idconsulta = ip.idconsulta)\n" +
                "                 GROUP BY c.idconsulta) as s\n" +
                "             INNER JOIN\n" +
                "\t\t\t\tconsulta cs ON (cs.idconsulta = s.idconsulta)\n" +
                "             INNER JOIN \n" +
                "            \tveterinario v ON (cs.idveterinario = v.idfuncionario)\n" +
                "             INNER JOIN \n" +
                "             \tcliente cl ON (cl.idcliente = cs.idcliente)";

        PreparedStatement comando = conexao.prepareStatement(sql);

        System.out.println("[ 5 ] Listar o valor dos produtos utilizados nas consultas, o valor " +
                "\n\t  total das consultas, o CRMV do veterinário que a realizou " +
                "\n\t  e o CPF do cliente (dono do animal).");


        //System.out.println('\n' + "Executando consulta: " + sql + '\n');

        ResultSet resultado = comando.executeQuery();

        System.out.format("| %20s | %20s | %20s | %20s | %20s | \n",
                "--------------------", "--------------------", "--------------------",
                "--------------------", "--------------------");

        System.out.format("| %20s | %20s | %20s | %20s | %20s | \n",
                "Número da Consulta", "CRMV", "CPF", "Valor da Consulta", "Valor dos Materiais" );

        System.out.format("| %20s | %20s | %20s | %20s | %20s | \n",
                "--------------------", "--------------------", "--------------------",
                "--------------------", "--------------------");

        while(resultado.next()){
            int idconsulta = resultado.getInt("idconsulta");
            String crmv = resultado.getString("crmv");
            Long cpf = resultado.getLong("cpf");
            String valor = resultado.getString("valor");
            String valor_materiais = resultado.getString("valor_materiais");
            System.out.printf("| %20d | %20s | %20s | %20s | %20s | \n",idconsulta, crmv, cpf,
                    valor, valor_materiais);
        }

    }

    public static void sextaConsulta(Connection conexao) throws SQLException{
        String sql = "SELECT \n" +
                "    idproduto,\n" +
                "    nome,\n" +
                "    descricao,\n" +
                "    qtd_itens,\n" +
                "    (val_revenda - val_compra) AS lucro_item,\n" +
                "    CAST(qtd_itens as int) * (val_revenda - val_compra) AS lucro_total\n" +
                "FROM\n" +
                "    produto\n" +
                "ORDER BY lucro_item DESC";

        PreparedStatement comando = conexao.prepareStatement(sql);

        System.out.println("[ 6 ] Listar o nome, a descrição, a quantidade dos produtos, o lucro " +
                "\n\t  por item e o lucro total dos mesmos.");


        //System.out.println('\n' + "Executando consulta: " + sql + '\n');

        ResultSet resultado = comando.executeQuery();

        System.out.format("| %10s | %30s | %50s | %15s | %20s | %20s | \n",
                "----------", "-----------------------------", "-------------------------------------------------",
                "---------------", "--------------------", "--------------------");


        System.out.format("| %10s | %30s | %50s | %15s | %20s | %20s | \n",
                "Nº produto", "Nome", "Descrição", "Qtd. de Itens", "Lucro por Item", "Lucro Total" );

        System.out.format("| %10s | %30s | %50s | %15s | %20s | %20s | \n",
                "----------", "-----------------------------", "-------------------------------------------------",
                "---------------", "--------------------", "--------------------");

        while(resultado.next()){
            int idproduto = resultado.getInt("idproduto");
            String nome = resultado.getString("nome");
            String descricao = resultado.getString("descricao");
            int qtd_itens = resultado.getInt("qtd_itens");
            String lucro_item = resultado.getString("lucro_item");
            String lucro_total = resultado.getString("lucro_total");
            System.out.printf("| %10d | %30s | %50s | %15d | %20s | %20s | \n",idproduto, nome, descricao,
                    qtd_itens, lucro_item, lucro_total);
        }

    }

    public static void setimaConsulta(Connection conexao) throws SQLException{
        String sql = "SELECT \n" +
                "    cargo, SUM(salario) AS soma_salario\n" +
                "FROM\n" +
                "    funcionario\n" +
                "GROUP BY cargo\n" +
                "HAVING SUM(salario) > CAST (15000 as money)\n" +
                "ORDER BY soma_salario";

        PreparedStatement comando = conexao.prepareStatement(sql);

        System.out.println("[ 7 ] Listar o gasto mensal com salários de todos os cargos da clínica.");

        //System.out.println('\n' + "Executando consulta: " + sql + '\n');

        ResultSet resultado = comando.executeQuery();

        System.out.format("| %20s | %20s | \n",
                "--------------------", "--------------------");

        System.out.format("| %20s | %20s | \n",
                "Cargo", "Salário Total" );

        System.out.format("| %20s | %20s | \n",
                "--------------------", "--------------------");

        while(resultado.next()){
            String cargo = resultado.getString("cargo");
            String soma_salario = resultado.getString("soma_salario");
            System.out.printf("| %20s | %20s | \n",cargo, soma_salario);
        }

    }

    public static void oitavaConsulta(Connection conexao) throws SQLException{
        String sql = "SELECT \n" +
                "    f.idfuncionario,\n" +
                "    f.pnome,\n" +
                "    f.snome,\n" +
                "    v.crmv,\n" +
                "    a.data_cadastro,\n" +
                "    h.idhistorico,\n" +
                "    h.idconsulta\n" +
                "FROM\n" +
                "    veterinario v\n" +
                "        JOIN\n" +
                "    funcionario f USING (idfuncionario)\n" +
                "        JOIN\n" +
                "    acesso a ON (a.idveterinario = f.idfuncionario)\n" +
                "        INNER JOIN\n" +
                "    historico h ON (a.idhistorico = h.idhistorico)\n" +
                "WHERE\n" +
                "    idfuncionario IN (SELECT \n" +
                "            idveterinario\n" +
                "        FROM\n" +
                "            acesso\n" +
                "        WHERE\n" +
                "            data_cadastro > '2017-04-04 10:46:00')\n";

        PreparedStatement comando = conexao.prepareStatement(sql);

        System.out.println("[ 8 ] Listar o nome completo e o CRMV dos veterinários, bem como data " +
                "\n\t  e hora de acesso ao histórico de consultas e o " +
                "\n\t  identificador das consultas.");

        //System.out.println('\n' + "Executando consulta: " + sql + '\n');

        ResultSet resultado = comando.executeQuery();

        System.out.format("| %20s | %20s | %20s | %20s | %20s | %20s | %20s | \n",
                "--------------------", "--------------------", "--------------------",
                "--------------------", "--------------------", "--------------------", "--------------------");

        System.out.format("| %20s | %20s | %20s | %20s | %20s | %20s | %20s | \n",
                "Nº do Veterinário", "Nome", "Sobrenome", "CRMV", "Data de Cadastro", "Nº do Histórico", "Nº da Consulta"  );

        System.out.format("| %20s | %20s | %20s | %20s | %20s | %20s | %20s | \n",
                "--------------------", "--------------------", "--------------------",
                "--------------------", "--------------------", "--------------------", "--------------------");

        while(resultado.next()){
            int idfuncionario = resultado.getInt("idfuncionario");
            String pnome = resultado.getString("pnome");
            String snome = resultado.getString("snome");
            String crmv = resultado.getString("crmv");
            Date data_cadastro = resultado.getDate("data_cadastro");
            int idhistorico = resultado.getInt("idhistorico");
            int idconsulta = resultado.getInt("idconsulta");
            System.out.printf("| %20d | %20s | %20s | %20s | %20s | %20d | %20d | \n",idfuncionario, pnome, snome, crmv, data_cadastro,
                    idhistorico, idconsulta);
        }

    }

    public static void nonaConsulta(Connection conexao) throws SQLException{
        String sql = "SELECT \n" +
                "    snome || ' ' || pnome AS dono,\n" +
                "    idanimal,\n" +
                "    nome AS animal,\n" +
                "    raca,\n" +
                "    especie\n" +
                "FROM\n" +
                "    (SELECT \n" +
                "        *\n" +
                "    FROM\n" +
                "        animal a\n" +
                "    INNER JOIN especie USING (idespecie)\n" +
                "    INNER JOIN cliente c ON (a.idcliente = c.idcliente)\n" +
                "    WHERE\n" +
                "        especie LIKE '%felino%') AS s";

        PreparedStatement comando = conexao.prepareStatement(sql);

        System.out.println("[ 9 ] Listar o nome completo do cliente, nome do animal e sua raça da " +
                "\n\t  espécie felino.");

        //System.out.println('\n' + "Executando consulta: " + sql + '\n');

        ResultSet resultado = comando.executeQuery();

        System.out.format("| %20s | %20s | %20s | %25s | %20s | \n",
                "--------------------", "--------------------", "--------------------",
                "-------------------------", "--------------------");

        System.out.format("| %20s | %20s | %20s | %25s | %20s |  \n",
                "Dono", "Nº do animal", "Nome do Animal", "Raça", "Espécie"  );

        System.out.format("| %20s | %20s | %20s | %25s | %20s | \n",
                "--------------------", "--------------------", "--------------------",
                "-------------------------", "--------------------");

        while(resultado.next()){
            String dono = resultado.getString("dono");
            int idanimal = resultado.getInt("idanimal");
            String animal = resultado.getString("animal");
            String raca = resultado.getString("raca");
            String especie = resultado.getString("especie");
            System.out.printf("| %20s | %20d | %20s | %25s | %20s |  \n",dono, idanimal, animal, raca, especie);
        }

    }

    public static void decimaConsulta(Connection conexao) throws SQLException{
        String sql = "WITH animais_atendidos as (    \n" +
                "SELECT \n" +
                "    avg(c.valor::numeric::float4) as valor_medio, c.idveterinario\n" +
                "FROM\n" +
                "    animal a\n" +
                "        INNER JOIN\n" +
                "    consulta c ON (c.idanimal = a.idanimal)\n" +
                "WHERE\n" +
                "    c.status = 'realizada'\n" +
                "GROUP BY c.idveterinario\n" +
                "ORDER BY c.idveterinario)\n" +
                "\n" +
                "\n" +
                "SELECT \n" +
                "    at.idveterinario,\n" +
                "    f.pnome || ' ' || f.snome AS veterinario,\n" +
                "    at.valor_medio\n" +
                "FROM\n" +
                "    animais_atendidos at\n" +
                "        INNER JOIN\n" +
                "    funcionario f ON (at.idveterinario = f.idfuncionario)";

        PreparedStatement comando = conexao.prepareStatement(sql);

        System.out.println("[ 10 ] Listar o valor médio das consultas por veterinário.");

        //System.out.println('\n' + "Executando consulta: " + sql + '\n');

        ResultSet resultado = comando.executeQuery();

        System.out.format("| %20s | %20s | %26s | \n",
                "--------------------", "--------------------", "------------------------");

        System.out.format("| %20s | %20s | %26s | \n",
                "Nº do Veterinário", "Nome", "Valor Médio" );

        System.out.format("| %20s | %20s | %26s | \n",
                "--------------------", "--------------------", "------------------------");

        while(resultado.next()){
            int idveterinario = resultado.getInt("idveterinario");
            String veterinario = resultado.getString("veterinario");
            String valor_medio = resultado.getString("valor_medio");
            System.out.printf("| %20d | %20s | R$%25.9s | \n",idveterinario, veterinario, valor_medio);
        }

    }

    public Connection connect() {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url, user, password);
            //System.out.println("Connected to the PostgreSQL server successfully.");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return conn;
    }

    public final static void clearConsole()
    {
        try
        {
            final String os = System.getProperty("os.name");

            if (os.contains("Windows"))
            {
                Runtime.getRuntime().exec("cls");
            }
            else
            {
                Runtime.getRuntime().exec("clear");
            }
        }
        catch (final Exception e)
        {
            //  Handle any exceptions.
        }
    }

    public static void menu(Connection conn, String option) {
        Scanner scanner = new Scanner(System.in);
        infinite_loop:
        while (true) {
            clearConsole();
            System.out.println("\u001B[1mTrabalho Prático de Banco de Dados - CONSULTAS\n\n\u001B[0m" +
                    "\u001B[1m[ 1 ]\u001B[0m Listar o nome completo, o CPF, a data de nascimento e o " +
                    "\n\t  telefone residencial dos clientes que nasceram depois de " +
                    "\n\t  1º de janeiro de 1980.\n\n" +
                    "\u001B[1m[ 2 ]\u001B[0m Listar o nome completo, o CRMV e o salário dos funcionários no " +
                    "\n\t  cargo de veterinário que ganham mais de R$ 6000.00.\n\n" +
                    "\u001B[1m[ 3 ]\u001B[0m Listar o identificador, o status e o resumo de todas as " +
                    "\n\t  consultas (realizadas e agendadas) dos veterinários com " +
                    "\n\t  CRMV  468415 e 237495 bem como seus nomes completos.\n\n" +
                    "\u001B[1m[ 4 ]\u001B[0m Listar os identificadores, os nomes reais e fantasia dos " +
                    "\n\t  fornecedores e os valores de todas as compras realizadas " +
                    "\n\t  de todos os itens fornecidos.\n\n" +
                    "\u001B[1m[ 5 ]\u001B[0m Listar o valor dos produtos utilizados nas consultas, o valor " +
                    "\n\t  total das consultas, o CRMV do veterinário que a realizou " +
                    "\n\t  e o CPF do cliente (dono do animal).\n\n" +
                    "\u001B[1m[ 6 ]\u001B[0m Listar o nome, a descrição, a quantidade dos produtos, o lucro " +
                    "\n\t  por item e o lucro total dos mesmos.\n\n" +
                    "\u001B[1m[ 7 ]\u001B[0m Listar o gasto mensal com salários de todos os cargos da clínica.\n\n" +
                    "\u001B[1m[ 8 ]\u001B[0m Listar o nome completo e o CRMV dos veterinários, bem como data " +
                    "\n\t  e hora de acesso ao histórico de consultas e o " +
                    "\n\t  identificador das consultas.\n\n" +
                    "\u001B[1m[ 9 ]\u001B[0m Listar o nome completo do cliente, nome do animal e sua raça da " +
                    "\n\t  espécie felino.\n\n" +
                    "\u001B[1m[ 10 ]\u001B[0m Listar o valor médio das consultas por veterinário.\n\n" +
                    "\u001B[1m[ 11 ]\u001B[0m SAIR  \n");


            option = scanner.nextLine();

            clearConsole();
            switch (option) {
                case "1":
                    System.out.println("");
                    try {
                        primeiraConsulta(conn);
                    } catch (SQLException e) {
                        System.out.println(e.getMessage());
                    }

                    System.out.println("\nPressione ENTER para voltar ao menu inicial.");
                    try {
                        System.in.read();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case "2":
                    System.out.println("");

                    try {
                        segundaConsulta(conn);
                    } catch (SQLException e) {
                        System.out.println(e.getMessage());
                    }

                    System.out.println("\nPressione ENTER para voltar ao menu inicial.");
                    try {
                        System.in.read();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case "3":
                    System.out.println("");

                    try {
                        terceiraConsulta(conn);
                    } catch (SQLException e) {
                        System.out.println(e.getMessage());
                    }

                    System.out.println("\nPressione ENTER para voltar ao menu inicial.");
                    try {
                        System.in.read();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case "4":
                    System.out.println("");

                    try {
                        quartaConsulta(conn);
                    } catch (SQLException e) {
                        System.out.println(e.getMessage());
                    }

                    System.out.println("\nPressione ENTER para voltar ao menu inicial.");
                    try {
                        System.in.read();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case "5":
                    System.out.println("");

                    try {
                        quintaConsulta(conn);
                    } catch (SQLException e) {
                        System.out.println(e.getMessage());
                    }

                    System.out.println("\nPressione ENTER para voltar ao menu inicial.");
                    try {
                        System.in.read();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case "6":
                    System.out.println("");

                    try {
                        sextaConsulta(conn);
                    } catch (SQLException e) {
                        System.out.println(e.getMessage());
                    }

                    System.out.println("\nPressione ENTER para voltar ao menu inicial.");
                    try {
                        System.in.read();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case "7":
                    System.out.println("");

                    try {
                        setimaConsulta(conn);
                    } catch (SQLException e) {
                        System.out.println(e.getMessage());
                    }

                    System.out.println("\nPressione ENTER para voltar ao menu inicial.");
                    try {
                        System.in.read();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case "8":
                    System.out.println("");

                    try {
                        oitavaConsulta(conn);
                    } catch (SQLException e) {
                        System.out.println(e.getMessage());
                    }

                    System.out.println("\nPressione ENTER para voltar ao menu inicial.");
                    try {
                        System.in.read();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case "9":
                    System.out.println("");

                    try {
                        nonaConsulta(conn);
                    } catch (SQLException e) {
                        System.out.println(e.getMessage());
                    }

                    System.out.println("\nPressione ENTER para voltar ao menu inicial.");
                    try {
                        System.in.read();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case "10":
                    System.out.println("");
                    try {
                        terceiraConsulta(conn);
                    } catch (SQLException e) {
                        System.out.println(e.getMessage());
                    }

                    System.out.println("\nPressione ENTER para voltar ao menu inicial.");
                    try {
                        System.in.read();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                case "11":
                    System.out.println("\n\u001B[1mFim\u001B[0m\n");
                    break infinite_loop;

                default:

                    System.out.println("\n\u001B[1mOpção inválida!\u001B[0m");
                    System.out.println("Pressione ENTER para voltar ao menu inicial.");
                    try {
                        System.in.read();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    public static void main(String[] args) {
        Main app = new Main();
        Connection conn = app.connect();

        int firstArg;
        if ((args.length > 0) && (args[0].equals("12"))) {
            try {
                firstArg = Integer.parseInt(args[0]);
                try {
                    primeiraConsulta(conn);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                System.out.println("\n");

                try {
                    segundaConsulta(conn);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                System.out.println("\n");

                try {
                    terceiraConsulta(conn);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                System.out.println("\n");

                try {
                    quartaConsulta(conn);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                System.out.println("\n");

                try {
                    quintaConsulta(conn);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                System.out.println("\n");

                try {
                    sextaConsulta(conn);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                System.out.println("\n");

                try {
                    setimaConsulta(conn);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                System.out.println("\n");

                try {
                    oitavaConsulta(conn);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                System.out.println("\n");

                try {
                    nonaConsulta(conn);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                System.out.println("\n");

                try {
                    decimaConsulta(conn);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                System.out.println("\n");

            } catch (NumberFormatException e) {
                System.err.println("Argument" + args[0] + " must be an integer.");
                System.exit(1);
            }
        } else {
            menu(conn, "");
        }

    }
}


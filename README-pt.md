# Projeto de Banco de Dados - 2016.2

**Objetivo**: criar uma aplicação utilizando a linguagem de programação Java, utilizando o JDBC, como visto em aula. 

A aplicação deve ser simples e mostrar o resultado das consultas criadas na etapa anterior do projeto. Deve ser impresso em tela a pergunta da consulta (Ex: Listar os alunos com média maior do que 7,0) e o resultado da consulta. O resultado deve ser apresentado de forma organizada. Caso o resultado seja mostrado como um texto plano, deve-se usar tabulação e quebras de linhas. Os tipos de cada um dos atributos resultantes da consulta também deverão ser formatados (Valores de data, dinheiro, números reais, entre outros, devem ser considerados).

## Execução da Aplicação

A aplicação pode ser executada interativamente no terminal (preferencialmente numa janela maximizada ou em tela cheia) utilizando o seguinte comando:

```bash
$ java -jar ProjetoBancodeDados.jar
```

Neste modo a aplicação fornecerá instruções de funcionamento durante sua execução. Um menu principal é exibido e nele são oferecidas 11 opções, sendo que as 10 primeiras são as consultas ao banco de dados e a última para saída.


A aplicação também oferece a opção de impressão do resultado das 10 consultas na tela, e, em conjunto com o operador ‘>’ a saída é direcionada para algo diferente do stdout num arquivo de texto definido. Isto pode ser feito utilizando o seguinte comando:

```bash
$ java -jar ProjetoBancodeDados.jar 12 > output.txt
```

## Resultado das consultas

##### [ 1 ] Listar o nome completo, o CPF, a data de nascimento e o telefone residencial dos clientes que nasceram depois de  1º de janeiro de 1980.
    
|               Código |                  Cpf |   Data de Nascimento |             Telefone | 
| -------------------- | -------------------- | -------------------- | -------------------- | 
|       Coby Blackburn |        1625022880299 |           1990-01-27 |       (79) 9676-9505 | 
|         Ronan Sawyer |        1659022626499 |           1980-07-22 |       (79) 9543-8416 | 
|    Katherine Justice |        1608093040099 |           1981-04-21 |       (79) 9886-0977 | 
|          Henry Olsen |        1675091245399 |           1983-04-07 |       (79) 9153-2809 | 
|          Justin Webb |        1666021147699 |           1985-06-23 |       (79) 9178-9516 | 
|          Castor Cash |        1643010735699 |           1982-06-18 |       (79) 9504-3091 | 
|      Taylor Espinoza |        1624020491399 |           1981-12-12 |       (79) 9657-5816 | 
|        Burton Bowers |        1631041383499 |           1981-02-13 |       (79) 9974-7489 | 


##### [ 2 ] Listar o nome completo, o CRMV e o salário dos funcionários no cargo de veterinário que ganham mais de R$ 6000.00.

|               Código |                 CRMV |              Salário | 
| -------------------- | -------------------- | -------------------- | 
|          Hayes Oneil |       CRMV-SE 237495 |          R$ 9.077,00 | 
|          Adam Golden |       CRMV-SE 468415 |          R$ 7.734,00 | 
|   Kennedy Washington |       CRMV-SE 567329 |          R$ 7.463,00 | 
|         Adrew Hooper |       CRMV-SE 036478 |          R$ 6.385,00 | 


##### [ 3 ] Listar o identificador, o status e o resumo de todas as consultas (realizadas e agendadas) dos veterinários com CRMV  468415 e 237495 bem como seus nomes completos.

|   Nº cons. |               Status |  Cód. do Veterinário |                 CRMV |                 Nome |            Sobrenome |               Resumo
| ---------- | -------------------- | -------------------- | -------------------- | -------------------- | -------------------- | --------------------
|          1 |            realizada |                    1 |       CRMV-SE 237495 |                Hayes |                Oneil | In vel mi sit amet augue congue elementum. Morbi in ipsum sit amet pede facilisis laoreet. Donec lacus nunc, viverra nec, blandit vel, egestas et, gue. Vestibulum tincidunt malesuada tellus. Ut ultrices ultrices enim. Curabitur sit amet mauris. Morbi in dui quis est pulvinar ullamcorper. Nulla facilisi. Integer lacinia sollicitudin massa. 
|          2 |            realizada |                    2 |       CRMV-SE 468415 |                 Adam |               Golden | Morbi in ipsum sit amet pede facilisis laoreet. Donec lacus nunc, viverra nec, blandit vel, egestas et, gue. Vestibulum tincidunt malesuada tellus. Ut ultrices ultrices enim. Curabitur sit amet mauris. Morbi in dui quis est pulvinar ullamcorper. Nulla facilisi. Integer lacinia sollicitudin massa. 
|          4 |             agendada |                    1 |       CRMV-SE 237495 |                Hayes |                Oneil |                      


##### [ 4 ] Listar os identificadores, os nomes reais e fantasia dos fornecedores e os valores de todas as compras realizadas  de todos os itens fornecidos.

| Número do Fornecedor |                 Nome Real |             Nome Fantasia |      Valor de Compra | 
| -------------------- | ------------------------- | ------------------------- | -------------------- | 
|                    1 |               Non Leo LLC |            Companhia Neon |            R$ 144,51 | 
|                    2 |        Commodo Ipsum Inc. |      Commodo Fornecedores |            R$ 289,03 | 
|                    3 |                 Proin LLC |       Proin Distribuidora |            R$ 241,90 | 
|                    4 |    Porttitor Incorporated |   Porttitor Distribuidora |             R$ 62,83 | 
|                    5 |          Ipsum Foundation |       Ipsum Distribuidora |            R$ 455,53 | 


##### [ 5 ] Listar o valor dos produtos utilizados nas consultas, o valor total das consultas, o CRMV do veterinário que a realizou e o CPF do cliente (dono do animal).

|   Número da Consulta |                 CRMV |                  CPF |    Valor da Consulta |  Valor dos Materiais | 
| -------------------- | -------------------- | -------------------- | -------------------- | -------------------- | 
|                    1 |       CRMV-SE 237495 |        1666021147699 |            R$ 637,11 |            R$ 337,09 | 
|                    2 |       CRMV-SE 468415 |        1625022880299 |            R$ 934,93 |            R$ 231,22 | 
|                    5 |       CRMV-SE 567329 |        1625022880299 |            R$ 720,05 |         R$ 11.000,00 | 
|                    3 |       CRMV-SE 567329 |        1625022880299 |            R$ 622,04 |            R$ 975,57 | 
|                    6 |       CRMV-SE 879651 |        1675091245399 |            R$ 795,45 |          R$ 9.000,00 | 


##### [ 6 ] Listar o nome, a descrição, a quantidade dos produtos, o lucro por item e o lucro total dos mesmos.

| Nº produto |                           Nome |                                          Descrição |   Qtd. de Itens |       Lucro por Item |          Lucro Total | 
| ---------- |  ----------------------------- |  ------------------------------------------------- | --------------- | -------------------- | -------------------- | 
|          4 |       ADEQID® 8.5mm  Microchip |                               Microchips para pets |              16 |             R$ 62,20 |            R$ 995,20 | 
|          3 |        ADEQID® 12mm  Microchip |                               Microchips para pets |              16 |             R$ 57,80 |            R$ 924,80 | 
|          5 |          Advantage II for Cats |                 Anti-pulgas e piolhos para felinos |              85 |             R$ 35,81 |          R$ 3.043,85 | 
|          2 | Terramycin Ophthalmic Ointment |         Antibiótico pomada para infecções oculares |              64 |             R$ 28,91 |          R$ 1.850,24 | 
|          1 |                  Sure Grow 100 |                   Suplemento para filhotes caninos |              92 |             R$ 12,57 |          R$ 1.156,44 | 


##### [ 7 ] Listar o gasto mensal com salários de todos os cargos da clínica.

|                Cargo |        Salário Total | 
| -------------------- | -------------------- | 
|           Secretaria |         R$ 19.798,00 | 
|          Veterinario |         R$ 36.626,00 | 


##### [ 8 ] Listar o nome completo e o CRMV dos veterinários, bem como data e hora de acesso ao histórico de consultas e o identificador das consultas.

|    Nº do Veterinário |                 Nome |            Sobrenome |                 CRMV |     Data de Cadastro |      Nº do Histórico |       Nº da Consulta | 
| -------------------- | -------------------- | -------------------- | -------------------- | -------------------- | -------------------- | -------------------- | 
|                    1 |                Hayes |                Oneil |       CRMV-SE 237495 |           2017-04-04 |                    4 |                    4 | 
|                    3 |              Kennedy |           Washington |       CRMV-SE 567329 |           2017-04-04 |                    5 |                    5 | 
|                    1 |                Hayes |                Oneil |       CRMV-SE 237495 |           2017-04-05 |                    1 |                    1 | 
|                    3 |              Kennedy |           Washington |       CRMV-SE 567329 |           2017-04-02 |                    3 |                    3 | 
|                   10 |                Carla |                 Pate |       CRMV-SE 879651 |           2017-04-24 |                    6 |                    6 | 


##### [ 9 ] Listar o nome completo do cliente, nome do animal e sua raça da espécie felino.

|                 Dono |         Nº do animal |       Nome do Animal |                      Raça |              Espécie |  
| -------------------- | -------------------- | -------------------- | ------------------------- | -------------------- | 
|          Cash Castor |                    7 |               Booboo |                Maine Coon |               felino |  
|         Sawyer Ronan |                   15 |                Acorn |                     Persa |               felino |  
|        Rocha Marcela |                    6 |             Choochoo |                    Siamês |               felino |  
|      Espinoza Taylor |                    8 |                Cisca |     Norueguês da Floresta |               felino |  
|        Bowers Burton |                    9 |               Claire |                     Persa |               felino |  
|     Hensley Samantha |                   10 |            Cleopatra |                       SRD |               felino |  
|          Webb Justin |                   12 |                Sushi |                    Siamês |               felino |  
|        Rocha Marcela |                   13 |                Sunny |                Maine Coon |               felino |  
|          Cash Castor |                   14 |               Summer |     Norueguês da Floresta |               felino |  


##### [ 10 ] Listar o valor médio das consultas por veterinário.

|    Nº do Veterinário |                 Nome |                Valor Médio | 
| -------------------- | -------------------- |   ------------------------ | 
|                    1 |          Hayes Oneil | R$               337.08999 | 
|                    2 |          Adam Golden | R$               231.22000 | 
|                    3 |   Kennedy Washington | R$               5987.7850 | 
|                   10 |           Carla Pate | R$                    9000 | 


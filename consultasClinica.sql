﻿
/* Consultas SQL */

SET search_path TO clinica_katharine_gabriel_paulo;


/* Consulta Simples */


SELECT pnome || ' ' || snome AS nome, cpf, data_nascimento, telefone_residencial
FROM cliente
WHERE data_nascimento > '1980-01-01';


/* Consultas Complexas */


-- JOIN de duas tabelas.

SELECT pnome || ' ' || snome AS nome, crmv, salario
FROM funcionario
  NATURAL JOIN veterinario
WHERE salario::numeric > 6000.00;


-- JOIN de várias tabelas variando o uso de NATURAL, USING e ON

-- Consulta 01 (UNION)

(SELECT
    c.idconsulta,
    c.status,
    c.idveterinario,
    v.crmv,
    f.pnome,
    f.snome,
    c.resumo
FROM
    consulta c
        INNER JOIN
    veterinario v ON c.idveterinario = v.idfuncionario
        INNER JOIN
    funcionario f ON c.idveterinario = f.idfuncionario
WHERE
    crmv = 'CRMV-SE 237495')
UNION (SELECT
	    c.idconsulta,
	    c.status,
	    c.idveterinario,
	    v.crmv,
	    f.pnome,
	    f.snome,
	    c.resumo
	FROM
	    consulta c
		INNER JOIN
	    veterinario v ON c.idveterinario = v.idfuncionario
		INNER JOIN
	    funcionario f ON c.idveterinario = f.idfuncionario
	WHERE
	    crmv = 'CRMV-SE 468415'
	);


-- Consulta 02 JOIN DE 3 TABELAS COM GROUPBY E ORDER BY

SELECT 
    f.idfornecedor,
    f.nome_real,
    f.nome_fantasia,
    SUM(val_compra)as valor
FROM
    produto p
        JOIN
    fornecedor_has_produto fhp USING (idproduto)
        INNER JOIN
    fornecedor f ON fhp.idfornecedor = f.idfornecedor
GROUP BY f.idfornecedor
ORDER BY f.idfornecedor;


-- valor dos itens utilizados na consulta

SELECT
    cs.idconsulta,
    v.crmv,
    cl.cpf,
    s.valor,
    cs.valor as valor_materiais
FROM
	(SELECT SUM(val_revenda) as valor, c.idconsulta
     	 FROM produto p
           INNER JOIN
              itemproduto ip ON (ip.idproduto = p.idproduto)
           INNER JOIN
               consulta c ON (c.idconsulta = ip.idconsulta)
           GROUP BY c.idconsulta) as s
       INNER JOIN
          consulta cs ON (cs.idconsulta = s.idconsulta)
       INNER JOIN
          veterinario v ON (cs.idveterinario = v.idfuncionario)
       INNER JOIN
          cliente cl ON (cl.idcliente = cs.idcliente);




-- Operações aritméticas + ORDER BY

SELECT 
    idproduto,
    nome,
    descricao,
    qtd_itens,
    (val_revenda - val_compra) AS lucro_item,
    CAST(qtd_itens as int) * (val_revenda - val_compra) AS lucro_total
FROM
    produto
ORDER BY lucro_item DESC;


-- usando HAVING

SELECT 
    cargo, SUM(salario) AS soma_salario
FROM
    funcionario
GROUP BY cargo
HAVING SUM(salario) > CAST (15000 as money)
ORDER BY soma_salario;


/* Consultas Aninhadas */

--  todos os veternarios que acessaram o historico em uma determinada data 
-- (podia ser feito bem mais simples mas ele quer aninhahada, nosso banco nao tem muitas possibilidades de consultas aninhadas)


-- subselect no WHERE

SELECT 
    f.idfuncionario,
    f.pnome,
    f.snome,
    v.crmv,
    a.data_cadastro,
    h.idhistorico,
    h.idconsulta
FROM
    veterinario v
        JOIN
    funcionario f USING (idfuncionario)
        JOIN
    acesso a ON (a.idveterinario = f.idfuncionario)
        INNER JOIN
    historico h ON (a.idhistorico = h.idhistorico)
WHERE
    idfuncionario IN (SELECT 
            idveterinario
        FROM
            acesso
        WHERE
            data_cadastro > '2017-04-04 10:46:00');



-- subselect no FROM + LIKE


SELECT 
    snome || ' ' || pnome AS dono,
    idanimal,
    nome AS animal,
    raca,
    especie
FROM
    (SELECT 
        *
    FROM
        animal a
    INNER JOIN especie USING (idespecie)
    INNER JOIN cliente c ON (a.idcliente = c.idcliente)
    WHERE
        especie LIKE '%felino%') AS s;


-- fazer uma com EXISTS ou WITH


WITH animais_atendidos as (
SELECT
    avg(c.valor::numeric::float4) as valor_medio, c.idveterinario
FROM
    animal a
        INNER JOIN
    consulta c ON (c.idanimal = a.idanimal)
WHERE
    c.status = 'realizada'
GROUP BY c.idveterinario
ORDER BY c.idveterinario
   )


SELECT
    at.idveterinario,
    f.pnome || ' ' || f.snome AS veterinario,
    at.valor_medio
FROM
    animais_atendidos at
        INNER JOIN
    funcionario f ON (at.idveterinario = f.idfuncionario);

DROP SCHEMA clinica CASCADE;

CREATE SCHEMA clinica-katharine-gabriel-paulo;

SET search_path TO clinica-katharine-gabriel-paulo;

CREATE TABLE cep (
    idcep SERIAL,
    estado VARCHAR(45),
    cidade VARCHAR(45),
    bairro VARCHAR(45),
    logradouro VARCHAR(150),
    cep character varying(15),
    PRIMARY KEY (idcep)
);


CREATE TABLE cliente (
    idcliente SERIAL,
    pnome VARCHAR(45),
    snome VARCHAR(45),
    data_nascimento DATE,
    telefone_residencial VARCHAR(45),
    telefone_celular VARCHAR(45),
    cpf VARCHAR(45) NOT NULL UNIQUE,
    email VARCHAR(100),
    profissao VARCHAR(45),
    numero INT,
    pais VARCHAR(45),
    idcep INT NOT NULL,
    PRIMARY KEY (idcliente),
    FOREIGN KEY (idcep) REFERENCES cep(idcep)
    ON DELETE SET NULL ON UPDATE CASCADE
);


CREATE TABLE especie (
    idespecie SERIAL,
    especie VARCHAR(45),
    raca VARCHAR(45),
    PRIMARY KEY (idespecie)
);


CREATE TABLE animal (
    idanimal SERIAL,
    nome VARCHAR(45),
    sexo VARCHAR(1),
    data_nascimento DATE,
    idcliente INT NOT NULL,
    idespecie INT NOT NULL,
    PRIMARY KEY (idanimal),
    FOREIGN KEY (idcliente) REFERENCES cliente(idcliente)
    ON DELETE SET NULL ON UPDATE CASCADE,
    FOREIGN KEY (idespecie) REFERENCES especie(idespecie)
    ON DELETE SET NULL ON UPDATE CASCADE
);

CREATE TABLE funcionario (
    idfuncionario SERIAL,
    pnome VARCHAR(45),
    snome VARCHAR(45),
    data_nascimento DATE,
    data_cadastro TIMESTAMP DEFAULT current_timestamp,
    telefone_residencial VARCHAR(45),
    telefone_celular VARCHAR(45),
    cpf VARCHAR(45) NOT NULL UNIQUE,
    email VARCHAR(100),
    cargo VARCHAR(45),
    salario MONEY,
    numero INT,
    pais VARCHAR(45),
    idcep INT NOT NULL, 
    PRIMARY KEY (idfuncionario),
    FOREIGN KEY (idcep) REFERENCES cep(idcep)
    ON DELETE SET NULL ON UPDATE CASCADE
);



CREATE TABLE recepcionista (
    idfuncionario SERIAL,
    PRIMARY KEY (idfuncionario),
    FOREIGN KEY (idfuncionario) REFERENCES funcionario(idfuncionario)
    ON DELETE SET NULL ON UPDATE CASCADE
);


CREATE TABLE secretaria (
    idfuncionario SERIAL,
    PRIMARY KEY (idfuncionario),
    FOREIGN KEY (idfuncionario) REFERENCES funcionario(idfuncionario)
    ON DELETE SET NULL ON UPDATE CASCADE
);



CREATE TABLE veterinario (
    idfuncionario SERIAL,
    crmv VARCHAR(45),
    idsecretaria INT NOT NULL,
    PRIMARY KEY (idfuncionario),
    FOREIGN KEY (idfuncionario) REFERENCES funcionario(idfuncionario)
    ON DELETE SET NULL ON UPDATE CASCADE,
    FOREIGN KEY (idsecretaria) REFERENCES secretaria(idfuncionario)
    ON DELETE SET NULL ON UPDATE CASCADE
);

CREATE TYPE status_consulta AS ENUM ('agendada', 'realizada');
CREATE TABLE consulta (
    idconsulta SERIAL,
    data_cadastro TIMESTAMP DEFAULT current_timestamp,
    data_marcacao TIMESTAMP DEFAULT current_timestamp,
    resumo VARCHAR(450),
    valor MONEY,
    status status_consulta,
    lista_prod VARCHAR(45),
    idcliente INT NOT NULL,
    idanimal INT NOT NULL,
    idsecretaria INT NOT NULL,
    idveterinario INT NOT NULL,
    PRIMARY KEY (idconsulta),
    FOREIGN KEY (idanimal) REFERENCES animal(idanimal)
    ON DELETE SET NULL ON UPDATE CASCADE,
    FOREIGN KEY (idcliente) REFERENCES cliente(idcliente)
    ON DELETE SET NULL ON UPDATE CASCADE,
    FOREIGN KEY (idsecretaria) REFERENCES secretaria(idfuncionario)
    ON DELETE SET NULL ON UPDATE CASCADE,
    FOREIGN KEY (idveterinario) REFERENCES veterinario(idfuncionario)
    ON DELETE SET NULL ON UPDATE CASCADE
);

CREATE TABLE historico (
    idhistorico SERIAL,
    idconsulta INT NOT NULL,
    data_cadastro TIMESTAMP DEFAULT current_timestamp,
    PRIMARY KEY (idhistorico),
    FOREIGN KEY (idconsulta) REFERENCES consulta(idconsulta)
    ON DELETE SET NULL ON UPDATE CASCADE
);


CREATE TABLE fornecedor (
    idfornecedor SERIAL,
    nome_real VARCHAR(45),
    nome_fantasia VARCHAR(45),
    data_cadastro DATE,
    email VARCHAR(100),
    responsavel VARCHAR(45),
    telefone VARCHAR(45),
    cnpj VARCHAR(45) NOT NULL UNIQUE,
    numero VARCHAR(45),
    pais VARCHAR(45),
    idcep INT NOT NULL,
    PRIMARY KEY (idfornecedor),
    FOREIGN KEY (idcep) REFERENCES cep(idcep)
    ON DELETE SET NULL ON UPDATE CASCADE
);


CREATE TABLE produto (
    idproduto SERIAL,
    nome VARCHAR(45),
    descricao VARCHAR(45),
    qtd_itens VARCHAR(45),
    val_compra MONEY,
    val_revenda MONEY,
    PRIMARY KEY (idproduto)
);

CREATE TABLE fornecedor_has_produto (
    idfornecedor SERIAL,
    idproduto INT NOT NULL,
    FOREIGN KEY (idfornecedor) REFERENCES fornecedor(idfornecedor)
    ON DELETE SET NULL ON UPDATE CASCADE,
    FOREIGN KEY (idproduto) REFERENCES produto(idproduto)
    ON DELETE SET NULL ON UPDATE CASCADE
);


CREATE TABLE itemproduto (
    iditemproduto SERIAL,
    tipo VARCHAR(45),
    idproduto INT NOT NULL,
    idconsulta INT NOT NULL,
    PRIMARY KEY (iditemproduto, idproduto),
    FOREIGN KEY (idconsulta) REFERENCES consulta(idconsulta)
    ON DELETE SET NULL ON UPDATE CASCADE,
    FOREIGN KEY (idproduto) REFERENCES produto(idproduto)
    ON DELETE SET NULL ON UPDATE CASCADE
);


CREATE TABLE perfil (
    idperfil SERIAL,
    login VARCHAR(45),
    senha VARCHAR(45),
    idfuncionario INT NOT NULL UNIQUE,
    PRIMARY KEY (idperfil), 
    FOREIGN KEY (idfuncionario) REFERENCES funcionario(idfuncionario)
    ON DELETE SET NULL ON UPDATE CASCADE
);


CREATE TABLE acesso (
    idacesso SERIAL,
    idhistorico INT NOT NULL,
    idperfil INT NOT NULL,
    idveterinario INT NOT NULL,
    data_cadastro TIMESTAMP DEFAULT current_timestamp,
    PRIMARY KEY (idacesso, idhistorico, idperfil),
    FOREIGN KEY (idhistorico) REFERENCES historico(idhistorico)
    ON DELETE SET NULL ON UPDATE CASCADE,
    FOREIGN KEY (idperfil) REFERENCES perfil(idperfil)
    ON DELETE SET NULL ON UPDATE CASCADE,
    FOREIGN KEY (idveterinario) REFERENCES veterinario(idfuncionario)
    ON DELETE SET NULL ON UPDATE CASCADE
);




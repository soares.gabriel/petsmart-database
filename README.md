# PetSmart Database 🐁🐈🐕

PetSmart is a custom management system for veterinary clinics. This repository contains the project part of the original application database.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
Give examples
```

## Deployment

Add additional notes about how to deploy this on a live system

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management
* [IntelliJ IDEA](https://www.jetbrains.com/idea/) - Tools for Java developers creating Java EE and Web applications
* [PostgreSQL](https://www.postgresql.org/) - Database Management System
* [JDK 8](http://www.oracle.com/technetwork/pt/java/javase/downloads/index.html) - Utilities to create software systems for the Java platform

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Katharine Padilha** - *Initial work* - [katharinepadilha](https://github.com/katharinepadilha)
* **Gabriel Soares** - *Initial work* - [golf-sierra](https://github.com/golf-sierra)
* **Paulo Roberto Morais** - *UI work* 

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone who's code was used
* Inspiration
* etc
